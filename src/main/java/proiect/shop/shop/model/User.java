package proiect.shop.shop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
// Asta face getter, setter, @ToString, @EqualsAndHashCode, @RequiredArgsConstructor
@Data
//constructori
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private Integer id;

    private String username;

    private String password;

    private String name;

    private String email;

    private boolean enabled;

    @ManyToOne(fetch=FetchType.LAZY)
    private Role role;







}

