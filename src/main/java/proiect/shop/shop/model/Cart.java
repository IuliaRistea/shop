package proiect.shop.shop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
//comentezi asta prima data cand rulezi
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    private Float total;

    @OneToOne()
    private Customer customer;

    //comentezi asta prima data cand rulezi
    @OneToMany(mappedBy = "cart",cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<CartItem> cart_items = new ArrayList<>();

    //Am sters order ID
/*    @OneToOne()
    private Orders order;*/

/*    public void addItem(CartItem item) {
        cart_items.add(item);
        item.setCart(this);*/
   // }
    //cum utilizez lombook
    //@Setter()private Integer cartID;

}
