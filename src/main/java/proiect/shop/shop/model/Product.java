package proiect.shop.shop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private Integer id;

    private String name;

    private String brand;

    private String country;

    private String description;

    private Float price;

    private boolean status;

    private Integer stock;

    private java.sql.Date expiry;

    @ManyToOne(fetch=FetchType.LAZY)
    private Category category;


}
