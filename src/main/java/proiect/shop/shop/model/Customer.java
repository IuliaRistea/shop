package proiect.shop.shop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private Integer id;

    private String phone;

    private String address;

    private java.sql.Date birthday;

//    @OneToOne(fetch=FetchType.LAZY)
//    private Cart cart;

    @OneToOne(fetch=FetchType.LAZY)
    private User user;

    @OneToOne(fetch=FetchType.LAZY)
    private Orders order;
}
