package proiect.shop.shop.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
//comentezi asta prima data cand rulezi
@Table(name = "cart_item")
public class CartItem {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    private Integer quantity;

    private Float total_price;

    //!!!!! Este posibil sa moara de aici
    //comentezi asta prima data cand rulezi
    @ManyToOne
    @JoinColumn(name = "cart_id", insertable = false, updatable = false)
    private Cart cart;
    private Integer cart_id; //partea asta nu trebuie comentata


    @OneToOne(fetch=FetchType.LAZY)
    private Product product;


/*    @ManyToMany(mappedBy = "cart_items")
    private List<Orders> order;*/



}
