package proiect.shop.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import proiect.shop.shop.model.CartItem;


import java.util.Optional;

@Repository
public interface CartItemDao extends JpaRepository<CartItem, Integer> {

}
