package proiect.shop.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import proiect.shop.shop.model.Orders;


import java.util.Optional;

@Repository
public interface OrderDao extends JpaRepository<Orders, Integer> {

}
