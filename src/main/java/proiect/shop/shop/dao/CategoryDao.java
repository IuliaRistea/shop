package proiect.shop.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import proiect.shop.shop.model.Category;

@Repository
public interface CategoryDao extends JpaRepository<Category, Integer> {

    //Category findCategoryById(Integer id);

}

