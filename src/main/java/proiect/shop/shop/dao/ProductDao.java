package proiect.shop.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.CrudRepositoryExtensionsKt;
import org.springframework.stereotype.Repository;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;

import java.util.Optional;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {
    
}

