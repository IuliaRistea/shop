package proiect.shop.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import proiect.shop.shop.model.Cart;
import proiect.shop.shop.model.CartItem;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.service.CartItemService;
import proiect.shop.shop.service.CartService;
import proiect.shop.shop.service.CategoryService;

import java.util.List;

@Controller
public class ShoppingCartController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    CartItemService cartItemService;

    // Functia pentru printat lista cu categorii in dropDown
    @GetMapping(value = "/shoppingCart")
    public String dropdownCategory(Model model) {

        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("dropCategories",categoryList);
        System.out.println(categoryList.toString());

        List<CartItem> cartItemList = cartItemService.findAll();
        model.addAttribute("cart_list",cartItemList);
        System.out.println(cartItemList.toString());

        float total_price = 0;
        for (CartItem cartItem: cartItemList) {
            total_price += cartItem.getTotal_price();
        }
        model.addAttribute("total_price",total_price);

        return "shoppingCart";
    }

    @PostMapping(value = "/shoppingCart/delete_item")
    public String deleteUserAccount(@RequestParam(name = "cart_item_id") Integer cart_item_id)
    {
        System.out.println("Deleting cart item with id: "  + cart_item_id.toString());
        cartItemService.deleteById(cart_item_id);
        return "redirect:/shoppingCart";
    }
}
