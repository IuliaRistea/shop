package proiect.shop.shop.controller;

import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import proiect.shop.shop.model.*;
import proiect.shop.shop.service.*;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ShopGridController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    CartService cartService;

    @Autowired
    CartItemService cartItemService;

    // Functia pentru printat lista cu categorii in dropDown
    @GetMapping(value = "/shopGrid")
    public String dropdownCategory(Model model) {

        List<Category> categoryList = categoryService.findAll();
        List<Product> productList = productService.findALl();

        model.addAttribute("dropCategories",categoryList);
        model.addAttribute("products", productList);

        System.out.println(categoryList.toString());
        return "shopGrid";
    }

    @RequestMapping(value = "/shopGrid/{categoryId}", method = RequestMethod.GET)
    public String findProductsbyCategory(@PathVariable("categoryId") int id, Model model)
    {
        System.out.println(id);
        List<Product> productList = productService.findALl();
        List<Product> productsWithCategory = new ArrayList<>();
        for (Product product : productList) {
            if (product.getCategory().getId() == id) {
                productsWithCategory.add(product);
            }
        }
        model.addAttribute("products", productsWithCategory);

        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("dropCategories",categoryList);

        return "shopGrid";

    }

    @PostMapping(value = "/shopGrid/add_product")
    public String addProduct(@RequestParam(name = "product_id") Integer productId,
                             @RequestParam(name = "quantity", required = false) Integer quantity_param)
    {
        CartItem cartItem = new CartItem();

        Product product = productService.getProductById(productId);
        float product_price = product.getPrice();

        int quantity = 1;
        if (quantity_param != null) {
            quantity = quantity_param.intValue();
        }
        System.out.println(quantity_param);
        cartItem.setQuantity(quantity);
        cartItem.setTotal_price(quantity * product_price);
        cartItem.setProduct(product);
        System.out.println(cartItem.toString());
        cartItemService.addCartItem(cartItem);
        return "redirect:/shoppingCart";
    }



}
