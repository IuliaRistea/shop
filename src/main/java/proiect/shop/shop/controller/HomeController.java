package proiect.shop.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    //Este necesar ca in momentul in care ajunge doar pe localhost sa te redirectioneze la indexul dinamic
    @GetMapping(value = "/")
    public String home() {
        return "redirect:/index";
    }

    @GetMapping(value = "/admin")
    public String admin() {return "redirect:/adminPage"; }

/*    @GetMapping(value = "/user")
    public String user() {return "index.html"; }*/

}
