package proiect.shop.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.service.CategoryService;

import java.util.List;

@Controller
public class CheckoutController {
    @Autowired
    CategoryService categoryService;

    // Functia pentru printat lista cu categorii in dropDown
    @GetMapping(value = "/checkout")
    public String dropdownCategory(Model model) {

        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("dropCategories",categoryList);

        System.out.println(categoryList.toString());
        return "checkout";
    }

}
