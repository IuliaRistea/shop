package proiect.shop.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import proiect.shop.shop.model.Role;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegisterController {

    @Autowired
    UserService userService;


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegistrationPage(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("register");
        return modelAndView;
    }


    @PostMapping("/register")
    public String registerUserAccount(@ModelAttribute("user") User user)
    {


        Role role = new Role();
        role.setLabel("USER");
        role.setId(2);


        user.setEnabled(true);
        user.setRole(role);

        //log
        System.out.println(user.toString() + "is being registered");

        userService.addUser(user);

        return "login.html";

        //return new ModelAndView("successRegister", "user", user);
    }

      /*@GetMapping("/register")
    public String showRegistrationForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "register.html";
    }*/

    /* @RequestMapping(value = "register", method = RequestMethod.POST)
    public ModelAndView processRegistrationForm(ModelAndView modelAndView, User user) {
        return modelAndView;
    }*/

}
