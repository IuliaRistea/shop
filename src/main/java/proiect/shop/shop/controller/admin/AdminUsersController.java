package proiect.shop.shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;
import proiect.shop.shop.model.Role;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.RoleService;
import proiect.shop.shop.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminUsersController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

//    @RequestMapping(value = "/adminUsers", method = RequestMethod.GET)
//    public ModelAndView showAdminUsersPage(ModelAndView modelAndView, User user,  Model model) {
//        // In fisierul HTML este referentiat ca th:object="${user}"
//        // Input object.
//        modelAndView.addObject("user", user);
//
//        List<User> userList = userService.findAll();
//        // In fisierul html este referentiat ca ${all_users}.
//        // Output object
//        model.addAttribute("all_users", userList);
//        System.out.println("User list :" + userList.toString());
//
//        // view name = numele fisierul HTML.
//        modelAndView.setViewName("adminUsers");
//        return modelAndView;
//    }

    @GetMapping(value= "/adminUsers")
    public String showAdminUsersPage(Model model) {
        List<User> userList = userService.findAll();
        model.addAttribute("all_users", userList);
        System.out.println(userList.toString());

        List<Role> roleList = roleService.findAll();
        System.out.println(roleList.toString());
        model.addAttribute("roles", roleList);
        return "adminUsers.html";
    }

    @PostMapping(value = "/adminUsers/addNew")
    public String addUser(User user,
                          @RequestParam(name = "role_id", required = false) String roleId)
    {
        Role role = new Role();
        if (roleId == "1") {
            role.setId(1);
            role.setLabel("ADMIN");
        }
        else {
            role.setId(2);
            role.setLabel("USER");
        }

        user.setRole(role);
        System.out.println(user.toString() + " TO BE ADDED");
        userService.addUser(user);
        return "redirect:/adminUsers";
    }

    @PostMapping(value = "/adminUsers/update")
    public String updateUser(User user,
                          @RequestParam(name = "role_id", required = false) String roleId)
    {
        Role role = new Role();
        if (roleId == "1") {
            role.setId(1);
            role.setLabel("ADMIN");
        }
        else {
            role.setId(2);
            role.setLabel("USER");
        }

        user.setRole(role);
        userService.deleteById(user.getId());
        userService.addUser(user);
        return "redirect:/adminUsers";
    }


    @PostMapping(value = "/adminUsers/delete")
    public String deleteUserAccount(@RequestParam(name = "user_id") Integer userId)
    {
        System.out.println("Deleting user with id: "  + userId.toString());
        userService.deleteById(userId);
        return "redirect:/adminUsers";
    }

}