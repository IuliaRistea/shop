package proiect.shop.shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import proiect.shop.shop.model.Customer;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.CustomerService;

import java.util.List;

@Controller
public class AdminCustomerController {

    @Autowired
    private CustomerService customerService;


    @GetMapping(value = "/adminCustomer")
    public String showCustomerPage(Model model) {
        List<Customer> customerList = customerService.findAll();
        model.addAttribute("all_customers", customerList);
        System.out.println(customerList.toString());
        return "adminCustomer";
    }

    @PostMapping(value = "/adminCustomer/addNew")
    public String addUser(Customer customer)
    {
        System.out.println(customer.toString() + " TO BE ADDED");
        customerService.addCustomer(customer);
        return "redirect:/adminCustomer";
    }

    @PostMapping(value = "/adminCustomer/delete")
    public String deleteCustomerAccount(@RequestParam(name = "customer_id") Integer customerId)
    {
        System.out.println("Deleting customer with id: "  + customerId.toString());
        customerService.deleteById(customerId);
        return "redirect:/adminCustomer";
    }
}