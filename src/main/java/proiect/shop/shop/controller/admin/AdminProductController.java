package proiect.shop.shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;
import proiect.shop.shop.service.CategoryService;
import proiect.shop.shop.service.ProductService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AdminProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;


    @GetMapping(value = "/adminProduct")
    public String showProduct(Model model) {

        List<Product> productList = productService.findALl();
        List<Category> categoryList = categoryService.findAll();

        model.addAttribute("products", productList);
        model.addAttribute("categories",categoryList);

        System.out.println(productList.toString());
        System.out.println(categoryList.size());

        return "adminProduct";
    }


    @PostMapping("/adminProduct/addNew")
    public String addProduct(Product product,
                             @RequestParam(name = "category_id", required = false) String category_id){
        List<Category> categoryList = categoryService.findAll();
        for (Category category : categoryList) {
            if (category.getId().toString().equals(category_id)) {
                Category product_category = new Category();
                product_category.setId(category.getId());
                product_category.setName(category.getName());
                product.setCategory(product_category);
                productService.saveProduct(product);
                return "redirect:/adminProduct";
            }
        }
        return "redirect:/adminProduct";
    }

    @PostMapping(value = "/adminProduct/update")
    public String updateProduct(Product product,
                             @RequestParam(name = "category_id", required = false) String category_id){
        productService.deleteById(product.getId());
        List<Category> categoryList = categoryService.findAll();
        for (Category category : categoryList) {
            if (category.getId().toString().equals(category_id)) {
                Category product_category = new Category();
                product_category.setId(category.getId());
                product_category.setName(category.getName());
                product.setCategory(product_category);
                productService.saveProduct(product);
                return "redirect:/adminProduct";
            }
        }
        return "redirect:/adminProduct";
    }

    @PostMapping(value = "/adminProduct/delete")
    public String deleteUserAccount(@RequestParam(name = "product_id") Integer productId)
    {
        System.out.println("Deleting product with id: "  + productId.toString());
        productService.deleteById(productId);
        return "redirect:/adminProduct";
    }


}