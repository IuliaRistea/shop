package proiect.shop.shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import proiect.shop.shop.model.Orders;
import proiect.shop.shop.service.*;

import java.util.List;


@Controller
public class AdminOrderController {

    @Autowired
    private OrdersService ordersService;

    @GetMapping(value= "/adminOrder")
    public String showAdminUsersPage(Model model) {
        List<Orders> userList = ordersService.findAll();
        model.addAttribute("all_orders", userList);
        System.out.println(userList.toString());

        return "adminOrders.html";
    }
}