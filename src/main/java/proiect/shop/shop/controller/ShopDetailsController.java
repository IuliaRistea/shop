package proiect.shop.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;
import proiect.shop.shop.service.CategoryService;
import proiect.shop.shop.service.ProductService;

import java.util.List;

@Controller
public class ShopDetailsController {
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductService productService;

/*    // Functia pentru printat lista cu categorii in dropDown
    @GetMapping(value = "/shopDetails")
    public String dropdownCategory(Model model) {

        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("dropCategories",categoryList);

        System.out.println(categoryList.toString());
        return "shopDetails";
    }*/

    @RequestMapping(value = "/shopDetails/{productId}", method = RequestMethod.GET)
    public String findProduct(@PathVariable("productId") int id, Model model)
    {
        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("dropCategories",categoryList);
        Product product = productService.getProductById(id);
        model.addAttribute("product",product);
        return "shopDetails";

    }


}
