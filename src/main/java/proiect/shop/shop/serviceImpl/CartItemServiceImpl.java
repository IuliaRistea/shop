package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.CartItemDao;
import proiect.shop.shop.model.CartItem;
import proiect.shop.shop.service.CartItemService;

import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    private CartItemDao cartItemDao;

    @Transactional(readOnly = true)
    @Override
    public List<CartItem> findAll() {
        return cartItemDao.findAll();
    }

    @Transactional()
    @Override
    public void deleteById(Integer cartItemId) {
        System.out.println("Cart item with id "+ cartItemId.toString() + " is being deleted from the table");
        cartItemDao.deleteById(cartItemId);
    }

    @Transactional()
    @Override
    public void addCartItem(CartItem cartItem){

        cartItemDao.save(cartItem);

    }

}
