package proiect.shop.shop.serviceImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.UserDao;
import proiect.shop.shop.model.Role;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.UserService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    //@Autowired
    private final UserDao usersDao;

    @Transactional()
    @Override
    public User addUser(User user) {
        System.out.println(user.toString() + " is being added to the table");
        return usersDao.save(user);
    }

    @Transactional()
    @Override
    public void deleteById(Integer user_id) {
        System.out.println("User with id "+ user_id.toString() + " is being deleted from the table");
        usersDao.deleteById(user_id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll() {
        return usersDao.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public User findUserByusername(String username) {

        return usersDao.findUserByusername(username);
    }

}