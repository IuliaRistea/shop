package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.CartDao;
import proiect.shop.shop.model.Cart;
import proiect.shop.shop.service.CartService;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {

        @Autowired
        private CartDao cartDao;

        @Transactional(readOnly = true)
        @Override
        public List<Cart> findAll(){return cartDao.findAll();}

}
