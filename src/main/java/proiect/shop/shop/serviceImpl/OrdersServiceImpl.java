package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.OrderDao;
import proiect.shop.shop.model.Orders;
import proiect.shop.shop.service.OrdersService;

import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrderDao orderDao;


    @Transactional(readOnly = true)
    @Override
    public List<Orders> findAll() {
        return orderDao.findAll();
    }
}
