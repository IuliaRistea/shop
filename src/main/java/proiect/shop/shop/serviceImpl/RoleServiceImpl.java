package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.RoleDao;
import proiect.shop.shop.model.Role;
import proiect.shop.shop.service.RoleService;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    public Role findById(int role_id) {
        Optional<Role> role = roleDao.findById(role_id);
        return role.get();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }
}
