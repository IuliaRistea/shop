package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.ProductDao;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;
import proiect.shop.shop.service.ProductService;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {


    @Autowired
    private ProductDao productDao;


    //Return list of products
    @Transactional(readOnly = true)
    @Override
    public List<Product> findALl() {
        return productDao.findAll();
    }

    //Save new product
    public void saveProduct(Product product){
        System.out.println(product.toString() + " is being added to the table");
        productDao.save(product);
    }


    //Get by Id
    public Optional<Product> findById(int id){ return productDao.findById(id);}

    //delete
    public void deleteById (Integer id){productDao.deleteById(id);}

    public Product getProductById(int id){
        return productDao.findById(id).orElse(null);
    }


}
