package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.CustomerDao;
import proiect.shop.shop.dao.UserDao;
import proiect.shop.shop.model.Customer;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.CustomerService;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Transactional()
    @Override
    public void addCustomer(Customer customer) {
        System.out.println(customer.toString() + " is being added to the table");
        customerDao.save(customer);
    }

    @Transactional()
    @Override
    public void deleteById(Integer customerId) {
        System.out.println("Customer with id "+ customerId.toString() + " is being deleted from the table");
        customerDao.deleteById(customerId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Customer> findAll() {
        return customerDao.findAll();
    }

}