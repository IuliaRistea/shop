package proiect.shop.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proiect.shop.shop.dao.CategoryDao;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;
import proiect.shop.shop.service.CategoryService;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Transactional(readOnly = true)
    @Override
    public List<Category> findAll() {
        return categoryDao.findAll();
    }

    //Save new product
    public void saveCategory(Category category){
        System.out.println(category.toString() + " is being added to the table");
        categoryDao.save(category);
    }

    //Get by id
    public Optional<Category> findById(int id){ return categoryDao.findById(id);}

    //Delete
    public void delete (Integer id){ categoryDao.deleteById(id);}
}
