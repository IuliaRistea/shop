package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.Category;

import java.util.List;
import java.util.Optional;

@Service
public interface CategoryService {

    List<Category> findAll();

    void saveCategory(Category category);

    Optional<Category> findById(int id);

    void delete (Integer id);
}
