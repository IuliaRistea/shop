package proiect.shop.shop.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import proiect.shop.shop.model.User;

import java.util.List;

@Service
//@RequiredArgsConstructor
public interface UserService {

    // Remember to set all user fields otherwise the front-end crashes due null pointer exception.
    User addUser(User user);

    List<User> findAll();

    User findUserByusername(String username);

    void deleteById(Integer user_id);
}