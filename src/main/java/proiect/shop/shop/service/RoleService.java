package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.Role;

import java.util.List;

@Service
public interface RoleService {

    Role findById(int role_id);

    List<Role> findAll();
}
