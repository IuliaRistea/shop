package proiect.shop.shop.service;

import org.springframework.stereotype.Service;

import proiect.shop.shop.model.Cart;

import java.util.List;

@Service
public interface CartService {

     List<Cart> findAll();


}
