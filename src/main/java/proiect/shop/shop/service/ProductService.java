package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.Category;
import proiect.shop.shop.model.Product;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductService {

    List<Product> findALl();

    void saveProduct(Product product);

    Optional<Product> findById(int id);

    void deleteById (Integer id);

    Product getProductById(int id);



}
