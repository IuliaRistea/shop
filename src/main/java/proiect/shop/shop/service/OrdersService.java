package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.Orders;

import java.util.List;

@Service
public interface OrdersService {

    List<Orders> findAll();
}
