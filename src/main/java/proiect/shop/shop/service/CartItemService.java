package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.CartItem;

import java.util.List;

@Service
public interface CartItemService {

    void addCartItem(CartItem cartItem);

    List<CartItem> findAll();

    void deleteById(Integer cartItemId);
}
