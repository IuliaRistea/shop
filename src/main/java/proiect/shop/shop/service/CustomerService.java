package proiect.shop.shop.service;

import org.springframework.stereotype.Service;
import proiect.shop.shop.model.Customer;
import proiect.shop.shop.model.User;

import java.util.List;

@Service
public interface CustomerService {
    // Remember to set all customer fields otherwise the front-end crashes due null pointer exception.
    void addCustomer(Customer customer);

    List<Customer> findAll();

    void deleteById(Integer customerId);
}
