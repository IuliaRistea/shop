$('document').ready(function () {

    $('.table #editButton').on('click', function (event) {
        event.preventDefault();

        var href = $(this).attr('href');

        jQuery.get(href,function (product, status) {
            $('#idEdit').val(product.id);
            $('#brandEdit').val(product.brand);
            $('#countryEdit').val(product.country);
            $('#descriptionEdit').val(product.description);
            $('#expiryEdit').val(product.expiry);
            $('#nameEdit').val(product.name);
            $('#priceEdit').val(product.price);
            $('#statusEdit').val(product.status);
            $('#stockEdit').val(product.stock);
            $('#ddlCategoryEdit').val(product.category_id);
        })
        $('modalEdit').modal();

    })

})