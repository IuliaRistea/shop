package proiect.shop.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import proiect.shop.shop.dao.UserDao;
import proiect.shop.shop.model.User;
import proiect.shop.shop.service.UserService;
import proiect.shop.shop.serviceImpl.UserServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserDao userDao = Mockito.mock(UserDao.class);

    private UserService userService;

    @BeforeEach
    void initUseCase() {
        userService = new UserServiceImpl(userDao);
    }

    @Test
    void savedUserSuccessfully() {
        User user = new User();
        user.setId(1);
        user.setUsername("test_username");
        user.setName("test_name");
        when(userDao.save(any(User.class))).then(returnsFirstArg());
        User savedUser = userService.addUser(user);
        assertThat(savedUser).isEqualTo(user);
        System.out.println("UserServiceTest passed");
    }

}