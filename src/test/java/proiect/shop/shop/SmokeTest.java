package proiect.shop.shop;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import proiect.shop.shop.controller.*;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private IndexController indexController;

    @Autowired
    private RegisterController registerController;

    @Autowired
    private LoginController loginController;

    @Autowired
    private CartController cartController;

    @Autowired
    private HomeController homeController;

    @Autowired
    private ShopGridController shopGridController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(indexController).isNotNull();
        assertThat(registerController).isNotNull();
        assertThat(loginController).isNotNull();
        assertThat(homeController).isNotNull();
        assertThat(shopGridController).isNotNull();
        System.out.println("SmokeTest passed ");
    }
}