CREATE SCHEMA shop;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`
(	
	`user_id` int NOT NULL AUTO_INCREMENT,
    `username` varchar(128) NOT NULL,
    `password` varchar(128) NOT NULL,
	`name` varchar(255)  NOT NULL,
	`email` varchar(128)  NOT NULL,
    `role_id` int,
    `enabled` tinyint(1) ,
	PRIMARY KEY (`user_id`)
);


DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer`
(
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `phone` varchar(20)  DEFAULT NULL 	,
  `address` varchar(255)  DEFAULT NULL,
  `birth_date` date NOT NULL,
  `cart_id` int,
  `user_id`int,
  PRIMARY KEY (`customer_id`)
);


ALTER TABLE `customer`
  ADD CONSTRAINT FK_userCustomer FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`);

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category`
(
	`category_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255)  NOT NULL,
    PRIMARY KEY (`category_id`)
);


DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product`
(
	`product_id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(255)  DEFAULT NULL,
    `brand` varchar(255) DEFAULT NULL,
    `country` varchar(255),
    `description` text,
    `price` float NOT NULL,
    `status`  tinyint(1) NOT NULL,
    `stock` int NOT NULL,
	`expiry` date,
	`category_id` int NOT NULL,

    
    PRIMARY KEY (`product_id`),
     CONSTRAINT FK_categoryProduct FOREIGN KEY (`category_id`) REFERENCES `category`(`category_id`)
);

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart`
(
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `total` float NOT NULL,
  `customer_id` int DEFAULT NULL,
  `cart_item_id` int NOT NULL,
   PRIMARY KEY (`cart_id`),
   CONSTRAINT FK_customercart FOREIGN KEY (`customer_id`) REFERENCES `customer`(`customer_id`)
);


ALTER TABLE `customer`
  ADD CONSTRAINT FK_cartCustomer FOREIGN KEY (`cart_id`) REFERENCES `cart`(`cart_id`);

DROP TABLE IF EXISTS `cartitem`;
CREATE TABLE `cartitem` (
  `cart_item_id` int NOT NULL AUTO_INCREMENT,
  `quantity` int NOT NULL,
  `total_price` float NOT NULL,
  `cart_id` int DEFAULT NULL,
  `product_id`int DEFAULT NULL,
  PRIMARY KEY (`cart_item_id`),
  CONSTRAINT FK_ProductCartitem FOREIGN KEY (product_id)REFERENCES product(product_id),
  CONSTRAINT FK_CartCartitem FOREIGN KEY (cart_id)REFERENCES cart(cart_id)
);

ALTER TABLE `cart`
  ADD CONSTRAINT FK_cartItemCart FOREIGN KEY (`cart_item_id`) REFERENCES `cartitem`(`cart_item_id`);


DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order`
(
	`order_id` int NOT NULL AUTO_INCREMENT,
	`cart_id`  int NOT NULL,
	`customer_id` int NOT NULL,
    PRIMARY KEY (`order_id`),
    CONSTRAINT FK_CartOrder FOREIGN KEY (cart_id)REFERENCES cart(cart_id),
    CONSTRAINT FK_CustomerOrder FOREIGN KEY (customer_id)REFERENCES customer(customer_id)
    );
    
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (

  `role_id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(16) NOT NULL,
  PRIMARY KEY (`role_id`)
  
);

ALTER TABLE `user`
  ADD CONSTRAINT FK_roleUsers FOREIGN KEY (`role_id`) REFERENCES `role`(`role_id`);

INSERT INTO category(category_id, name)
VALUES 
	(1,'Carne proaspata'),
    (2,'Legume'),
    (3,'Fructe'),
    (4,'Fructe de padure'),
    (5,'Fructe de mare'),
	(6,'Peste'),
    (7,'Nuci'),
	(8,'Lactate');

INSERT INTO product(product_id, brand, country, description, expiry, name, price, status, stock, category_id)
VALUES 		(1, 'Matache_macelaru','romania','carne proaspata', '2020-09-24', 'porc','50',true, 5, 1);


INSERT INTO product(id, brand, country, description, expiry, name, price, status, stock, category_id)
VALUES 		(1, 'Matache_macelaru','romania','carne proaspata', '2020-09-24', 'porc','50',true, 5, 1),
              (2, 'Dabuleni','Ungaria','cartofi autohtoni', null, 'cartof','10',false, 0, 2),
              (3,'Narcisel','Elvetia','zmeura de calitate superioara', '2021-09-22','zmeura','25',true, 1, 4);


INSERT INTO role(role_id, label)
VALUES 	(1, 'admin'),
		(2, 'client');

INSERT INTO user(user_id, username, password, name, email, role_id, enabled)
VALUES (1, 'Iulia', 'parola', 'Gemene', 'iuliaristea@gmail.com', 1, true),
	   (2, 'Narcis', 'parolica', 'Ristea', 'narcisgemene96@gmail.com',2 ,true);
       
INSERT INTO customer(customer_id, phone, address, birth_date, cart_id, user_id)
VALUES (1, '+43789752344', 'Str. Sperantei, nr 7, parter', '1996-08-30',null, 2); 


create user 'iulia'@'%' identified by 'pass'; -- Creates the user
create user 'root'@'%' identified by 'pass'; -- Creates the user
SET GLOBAL time_zone = '+1:00';
CREATE SCHEMA shop;
GRANT ALL PRIVILEGES ON shop.* TO 'root'@'%';
GRANT ALL PRIVILEGES ON shop.* TO 'iulia'@'%';
FLUSH PRIVILEGES;

SHOW tables;


INSERT INTO product(product_id, brand, country, description, expiry, name, price, status, stock, category_id) VALUES   (1, 'Matache_macelaru','romania','carne proaspata', '2020-09-24', 'porc','50',true, 5, 1),    (2, 'Dabuleni','Ungaria','cartofi autohtoni', 'cartof', null,'10',false, 0, 2),    (3,'Narcisel','Elvetia','zmeura de calitate superioara', 'zmeura', '2021-09-22','25',true, 1, 4)
